<?php


	Class Student {
		
		
		var $id = 12019291;
		var $firstname = "Christine";
		var $middlename = "Riva";
		var $lastname = "Rosero";
	    var $birthdate = "09-15-1998";
		var $sex = "Female";
		var $address = "Naga City";
		var $yearlevel=3;
		
		
		public function __construct() {
		}	
		
		public function getId() {
			return $this->id;
		}
		
		public function setId($_id) {
			$this->id = $_id;
		
		}
		
		
		
		public function getFirstname() {
			return $this->firstname;
		}
		
		public function setFirstname($_firstname) {
			$this->firstname = $_firstname;
		
		}
		
		
		public function getMiddlename() {
			return $this->middlename;
		}
		
		public function setMiddlename($_middlename) {
			$this->middlename = $_middlename;
		
		}
		
		
		public function getLastname() {
			return $this->lastname;
		}
		
		public function setLastname($_lastname) {
			$this->lastname = $_lastname;
		
		}
		
		
		public function getBirtdate() {
			return $this->birthdate;
		}
		
		public function setBirthdate($_birthdate) {
			$this->birthdate = $_birthdate;
		
		}
		
		
		public function getSex() {
			return $this->sex;
		}
		
		public function setSex($_sex) {
			$this->sex = $_sex;
		
		}
		
		
		public function getAddress() {
			return $this->address;
		}
		
		public function setAddress($_address) {
			$this->address = $_address;
		
		}
		
		
		public function getYearlevel() {
			return $this->yearlevel;
		}
		
		public function setYearlevel($_yearlevel) {
			$this->yearlevel = $_yearlevel;
		
		}
		
		
		
		
	}

?>
